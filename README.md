# Libros-Compartidos-Maestria-2023
## Estado
En proceso

## Colaboradores
- Edison López (1)
## Objetivo

Compartir libros en pdf sobre temas tecnológicos orientado a lo que estamos estudiando. 
Hay excelentes libros que se puede adquirir en su versión pdf, podemos coincidir que algunos compañeros también quieren adquirir el mismo titulo, pero sería mejor que sólo uno lo compre y el resto podemos adquirir otro, de esta manera optimizamos recursos y podemos llegar a tener una mini biblioteca entre el aporte de todos.    
## Libros compartidos 
- CLEAN CODE - A Handbook of Agile Software Craftsmanship (Robert C. Martin, Editorial ANAYA, 2012, Versión Español)
- HEAD FIRST DESIGN PATTERNS (Freeman, O`Reilly, 2004)
- THE SOFTWARE ARCHITECT ELEVATOR (Gregor Hohpe, O`Reilly, 2020)
- SUMERGETE EN LOS PATRONES DE DISEÑO (Alexander Shvets, ,2021)
- CLEAN ARCHITECTURE (Robert C. Martin, Prentice Hall, 2018)
- CLEAN CODE JAVASCRIPT (Miguel A. Gomez, Software Crafters, 2019)

## Libros candidatos
- INTRODUCCION A LOS PATRONES DE DISEÑO (Oscar Javier Blancarte Iturralde)
- INTRODUCCION A LA ARQUITECTURA DE SOFTWARE (Oscar Javier Blancarte Iturralde)
- Automating DevOs with GitLab CI/CD Pipelines (Cowell/  Lotz/ Timberlake, 2023)
- CODIGO SOSTENIBLE (Carlos Blé Jurado)
- CLEAN JAVASCRIPT (Miguel A. Gómez)
- INTRODUCTION TO THE TEAM SOFTWARE PROCESS (Watts S. Humphrey)
## Ultima actulización
25 de junio del 2023
